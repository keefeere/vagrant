    sudo apt-get update
    sudo apt-get install -y curl openssh-server ca-certificates

    debconf-set-selections <<< "postfix postfix/mailname string $HOSTNAME"
    debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
    DEBIAN_FRONTEND=noninteractive sudo apt-get install -y postfix
    if [ ! -e /vagrant/ubuntu-bionic-gitlab-ce_12.9.3-ce.0_amd64.deb ]; then
        wget --content-disposition -O /vagrant/ubuntu-bionic-gitlab-ce_12.9.3-ce.0_amd64.deb https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/bionic/gitlab-ce_12.9.3-ce.0_amd64.deb/download.deb
    fi
    sudo dpkg -i /vagrant/ubuntu-bionic-gitlab-ce_12.9.3-ce.0_amd64.deb
    #my edition - changing external_url to hostname
    sudo sed -i 's!^external_url .*!external_url "'http://$HOSTNAME'"!g' /etc/gitlab/gitlab.rb
    #for windows netbios name resolution
    sudo apt-get install -y  samba
    sudo gitlab-ctl reconfigure



# configure gitlab.
gitlab-ctl reconfigure

# set the gitlab root user password and create a personal access token.
# see https://gitlab.com/gitlab-org/gitlab-foss/blob/v13.6.1/app/models/user.rb
# see https://gitlab.com/gitlab-org/gitlab-foss/blob/v13.6.1/app/models/personal_access_token.rb
# see https://gitlab.com/gitlab-org/gitlab-foss/blob/v13.6.1/app/controllers/profiles/personal_access_tokens_controller.rb
gitlab-rails console -e production <<'EOF'
u = User.first
t = PersonalAccessToken.new({
    user: u,
    name: 'vagrant',
    scopes: ['api', 'read_user', 'sudo']})
t.save!
File.write(
    '/tmp/gitlab-root-personal-access-token.txt',
    t.token)
EOF
mkdir -p /vagrant/tmp
mv /tmp/gitlab-root-personal-access-token.txt /vagrant/tmp
