#set your pass and rename to sample.sh
gitlab-rails console -e production <<'EOF'
u = User.first
u.password_automatically_set = false
u.password = 'yourPass'
u.password_confirmation = 'yourPass'
u.save!
EOF