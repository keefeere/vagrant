**VAGRANTFILE for installing GitLab CE in VirtualBox with Vagrant**

![GitLab and Vagrant images](https://grafxflow.co.uk/storage/app/uploads/public/5ea/089/c63/thumb_1165_266_0_0_0_auto.png)

Using my notebook for installing GitLab CE with Vagrant
* set up *two* wirtual network - nat and bridged one
* set up shared folder
* used provision script to setup external_url populated with HOSTNAME
* in provision script changing root password and generating acces token and then moving to shared folder


